# S3 Explorer

S3 Explorer is a web based explorer for Amazon S3 (or other S3 compatible services).

It is inspired by [nolanlawson/pretty-s3-index-html][nolanlawson].

[Demo][demo]

## Screenshot

![screenshot](screenshoot/screenshot.png)

## Requirement

* A S3 bucket (or other S3 compatible services) that has public listing.

## Build
```bash
npm run build
```

This will create a build in `./dist`.

It support the following environment variables:

* `SHOW_INPUT`: Set to `true` to allow users to input their address.
* `API_URL`: Set the default address.
* `PUBLIC_PATH`: If you host this in a sub-folder instead of root, you should set it.
* `SHOW_RIBBON`: Show fork me on GitLab ribbon.

* `THEME_COLOR`: Set theme color of the application.

The variables can be set by `.env`, `.env.prod`, `--env.SHOW_INPUT=true`, etc.
See [.gitlab-ci.yml](.gitlab-ci.yml) for reference.


[demo]: https://s3-explorer.joshuaavalon.app/#/?api=https://nolanlawson.s3.amazonaws.com
[nolanlawson]: https://github.com/nolanlawson/pretty-s3-index-html

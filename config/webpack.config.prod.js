const { mergeEnv } = require("./utils");
const baseConfig = require("./webpack.config.base");
const webpackMerge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = (baseEnv, args) => {
  const env = mergeEnv(baseEnv, ".env.prod");
  return webpackMerge.smartStrategy({
    "module.rules.use": "prepend"
  })(baseConfig(env, args), {
    mode: "production",
    devtool: "source-map",
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/u,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                camelCase: true,
                modules: true,
                importLoaders: 2,
                sourceMap: true
              }
            }
          ]
        }
      ]
    },
    output: {
      filename: "js/[name].[contenthash].js",
      chunkFilename: "js/[name].[contenthash].js"
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: "css/[name].[contenthash].css",
        chunkFilename: "css/[name].[contenthash].css"
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          map: { inline: false },
          discardComments: { removeAll: true }
        }
      })
    ],
    optimization: {
      minimizer: [
        new TerserPlugin({
          parallel: true,
          cache: true,
          sourceMap: true,
          terserOptions: {
            ecma: 6,
            compress: true,
            output: {
              comments: false,
              beautify: false
            }
          }
        })
      ]
    }
  });
};

const { mergeEnv } = require("./utils");
const baseConfig = require("./webpack.config.base");
const webpackMerge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (baseEnv, args) => {
  const env = mergeEnv(baseEnv, ".env.dev");
  return webpackMerge.smartStrategy({
    "module.rules.use": "prepend"
  })(baseConfig(env, args), {
    mode: "development",
    devtool: "source-map",
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/u,
          use: [
            "css-hot-loader",
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                camelCase: true,
                modules: true,
                importLoaders: 2,
                sourceMap: true,
                localIdentName: "[path]-[hash:base64]"
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: "css/[name].css",
        chunkFilename: "css/[name].css"
      })
    ],
    output: {
      filename: "js/[name].[hash].js",
      chunkFilename: "js/[name].[hash].js"
    }
  });
};

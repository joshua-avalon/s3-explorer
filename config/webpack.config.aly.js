const baseConfig = require("./webpack.config.prod");
const webpackMerge = require("webpack-merge");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

module.exports = (env, args) =>
  webpackMerge.smartStrategy({})(baseConfig(env, args), {
    plugins: [new BundleAnalyzerPlugin()]
  });

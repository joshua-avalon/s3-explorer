const { resolvePath, mergeEnv } = require("./utils");
const webpack = require("webpack");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const WebpackPwaManifest = require("webpack-pwa-manifest");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = baseEnv => {
  const env = mergeEnv(baseEnv, ".env");
  const browsers = [
    "chrome 58",
    "firefox 54",
    "edge 14",
    "safari 10",
    "opera 55"
  ];
  const __THEME_COLOR__ = process.env.THEME_COLOR;
  const themeColor = __THEME_COLOR__;
  return {
    entry: {
      app: [resolvePath("src/index.tsx")]
    },
    module: {
      rules: [
        {
          test: /\.(tsx?|jsx?)$/u,
          include: /src/u,
          exclude: /node_modules/u,
          loader: "babel-loader"
        },
        {
          test: /\.html$/u,
          use: [
            {
              loader: "html-loader",
              options: {
                minimize: true
              }
            }
          ]
        },
        {
          test: /\.(png|jpg|gif|woff(2)?|eot|ttf|otf|svg)$/u,
          use: [
            {
              loader: "url-loader",
              options: {
                limit: 2048,
                fallback: "file-loader",
                name: "[path][name].[ext]",
                context: "src"
              }
            }
          ]
        },
        {
          test: /\.(sa|sc|c)ss$/u,
          use: [
            {
              loader: "postcss-loader",
              options: {
                ident: "postcss",
                sourceMap: true,
                plugins: [
                  require("postcss-preset-env")({
                    browsers
                  })
                ]
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true,
                includePaths: [resolvePath("src")],
                data: "$primary-color: " + themeColor + ";"
              }
            }
          ]
        },
        {
          test: /\.less$/u,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "postcss-loader",
              options: {
                ident: "postcss",
                sourceMap: true,
                plugins: [
                  require("postcss-preset-env")({
                    browsers
                  })
                ]
              }
            },
            {
              loader: "less-loader",
              options: {
                sourceMap: true,
                modifyVars: {
                  "primary-color": themeColor
                },
                javascriptEnabled: true
              }
            }
          ]
        }
      ]
    },
    output: {
      publicPath: env.PUBLIC_PATH
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/u, /moment$/u),
      new webpack.DefinePlugin(
        Object.keys(env).reduce(
          (prev, next) => {
            prev[`process.env.${next}`] = JSON.stringify(env[next]);
            return prev;
          },
          { __THEME_COLOR__ }
        )
      ),
      new CleanWebpackPlugin(),
      new CopyWebpackPlugin([
        {
          from: resolvePath("static"),
          ignore: ["android-chrome*.png"]
        }
      ]),
      new HtmlWebPackPlugin({
        template: resolvePath("static/index.html"),
        filename: "index.html",
        meta: {
          viewport: "width=device-width, initial-scale=1, shrink-to-fit=no",
          "theme-color": themeColor,
          "msapplication-TileColor": themeColor
        },
        favicon: resolvePath("static/favicon.ico")
      }),
      /* eslint-disable @typescript-eslint/camelcase */
      new WebpackPwaManifest({
        name: "S3 Explorer",
        short_name: "S3 Explorer",
        description: "Web based explorer for Amazon S3.",
        display: "standalone",
        theme_color: themeColor,
        background_color: "#ffffff",
        publicPath: env.PUBLIC_PATH,
        icons: [
          {
            src: resolvePath("static/android-chrome-192x192.png"),
            sizes: [192]
          },
          {
            src: resolvePath("static/android-chrome-512x512.png"),
            sizes: [512]
          }
        ]
      })
      /* eslint-enable @typescript-eslint/camelcase */
    ],
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
      alias: {
        "@": resolvePath("src/"),
        "@ant-design/icons/lib/dist$": resolvePath("src/antIcon.tsx")
      }
    },
    optimization: {
      splitChunks: {
        chunks: "all",
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/u,
            name: "vendors",
            reuseExistingChunk: true
          }
        }
      }
    }
  };
};

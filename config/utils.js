const path = require("path");
const fs = require("fs");
const dotenv = require("dotenv");

const resolvePath = relativePath => path.resolve(__dirname, "..", relativePath);

const exist = path => fs.existsSync(path);

const mergeEnv = (env, path) => {
  const envPath = resolvePath(path);
  if (exist(envPath)) {
    const fileEnv = dotenv.config({ path: envPath }).parsed;
    return { ...fileEnv, ...env };
  }
  return { ...env };
};

module.exports = {
  resolvePath,
  exist,
  mergeEnv
};

const path = require("path");
const fs = require("fs");

const resolvePath = relativePath => path.resolve(__dirname, "..", relativePath);

const html200 = resolvePath("dist/200.html");

if (fs.existsSync(html200)) {
  fs.unlinkSync(html200);
}

const critical = require("critical");

critical.generate({
  base: resolvePath("dist"),
  src: "index.html",
  target: "index.html",
  inline: true,
  minify: true,
  extract: true,
  penthouse: {
    blockJSRequests: false
  }
});

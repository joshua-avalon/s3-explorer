import React from "react";
import { hydrate, render } from "react-dom";

import { StoreProvider } from "@/store";
import { default as App } from "@/app";

const rootElement = document.getElementById("root") as HTMLElement;

const app = (
  <StoreProvider>
    <App />
  </StoreProvider>
);

if (rootElement.hasChildNodes()) {
  hydrate(app, rootElement);
} else {
  render(app, rootElement);
}

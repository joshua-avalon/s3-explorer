import { CommonPrefix, Content, ListBucketResult } from "./model";

const KB = 1024;
const MB = 1024 * KB;
const GB = 1024 * MB;

export default class Api {
  private readonly apiUrl: string;

  public constructor(apiUrl: string) {
    this.apiUrl = apiUrl;
  }

  public async list(
    params: { [key: string]: any } = {}
  ): Promise<ListBucketResult> {
    if (!params.delimiter) {
      params.delimiter = "/";
    }
    if (!params["list-type"]) {
      params["list-type"] = 2;
    }
    if (params.prefix === "") {
      params.prefix = undefined;
    }
    const url = `${this.apiUrl}/${this.queryString(params)}`;
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`${response.status}: ${response.statusText}`);
    }
    const xmlString = await response.text();
    const parser = new DOMParser();
    const doc = parser.parseFromString(xmlString, "text/xml");
    const name = this.getTextDefault(doc, "Name");
    const prefix = this.getText(doc, "Prefix");
    const maxKeys = this.getNumberDefault(doc, "MaxKeys");
    const keyCount = this.getNumberDefault(doc, "KeyCount");
    const delimiter = this.getText(doc, "Delimiter");
    const continuationToken = this.getText(doc, "ContinuationToken");
    const nextContinuationToken = this.getText(doc, "NextContinuationToken");
    const isTruncated = this.getBoolean(doc, "IsTruncated");
    const contents = this.getContentArray(doc, "Contents");
    const commonPrefixes = this.getCommonPrefixArray(doc, "CommonPrefixes");
    return {
      name,
      prefix,
      maxKeys,
      keyCount,
      delimiter,
      continuationToken,
      nextContinuationToken,
      isTruncated,
      contents,
      commonPrefixes
    };
  }

  private getCommonPrefixArray(doc: Document, tagName: string): CommonPrefix[] {
    const elements = doc.getElementsByTagName(tagName);
    const values: CommonPrefix[] = [];
    // tslint:disable-next-line prefer-for-of
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];
      const prefix = this.getText(element, "Prefix");
      if (prefix) {
        values.push({ prefix });
      }
    }

    return values;
  }

  private getContentArray(doc: Document, tagName: string): Content[] {
    const elements = doc.getElementsByTagName(tagName);
    const values: Content[] = [];
    const folderRegex = /_\$folder\$$/u;
    // tslint:disable-next-line prefer-for-of
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];
      const key = this.getTextDefault(element, "Key");
      const slice = key.split("/");
      const name = slice[slice.length - 1].replace(folderRegex, "");
      const nameSlice = name.split(".");
      const type =
        nameSlice.length > 1
          ? nameSlice[nameSlice.length - 1].toLowerCase()
          : undefined;
      const isFolder = folderRegex.test(key);
      const lastModified = this.getDateDefault(element, "LastModified");
      const eTag = this.getTextDefault(element, "ETag").replace(/"/gu, "");
      const size = this.getNumberDefault(element, "Size");
      const sizeStr = this.friendlySizeName(size);
      const storageClass = this.getTextDefault(element, "StorageClass");

      values.push({
        key,
        name,
        type,
        isFolder,
        lastModified,
        eTag,
        size,
        sizeStr,
        storageClass
      });
    }
    return values;
  }

  private friendlySizeName(size: number): string {
    if (size < KB) {
      return size + " B";
    } else if (size < MB) {
      return (size / KB).toFixed(0) + " KB";
    } else if (size < GB) {
      return (size / MB).toFixed(2) + " MB";
    }
    return (size / GB).toFixed(2) + " GB";
  }

  private getTextDefault(
    doc: Document | Element,
    tagName: string,
    defaultValue: string = ""
  ): string {
    const value = this.getText(doc, tagName);
    return value ? value : defaultValue;
  }

  private getDateDefault(
    doc: Document | Element,
    tagName: string,
    defaultValue: Date = new Date()
  ): Date {
    const text = this.getText(doc, tagName);
    if (!text) {
      return defaultValue;
    }
    return new Date(text);
  }

  private getNumberDefault(
    doc: Document | Element,
    tagName: string,
    defaultValue: number = 0
  ): number {
    const text = this.getText(doc, tagName);
    if (!text) {
      return defaultValue;
    }
    const value = Number(text);
    return value ? value : defaultValue;
  }

  private getBoolean(doc: Document | Element, tagName: string): boolean {
    const text = this.getText(doc, tagName);
    return text === "true";
  }

  private getText(
    doc: Document | Element,
    tagName: string
  ): string | undefined {
    const firstElement = doc.getElementsByTagName(tagName)[0];
    if (!firstElement) {
      return undefined;
    }
    const value = firstElement.textContent;
    return value ? value : undefined;
  }

  private queryString(params: { [key: string]: any }): string {
    const paramsList = [];
    for (const key in params) {
      if (!params.hasOwnProperty(key)) {
        continue;
      }
      const value = params[key];
      if (value === null || value === undefined) {
        continue;
      }
      const encodeKey = encodeURIComponent(key);
      const encodeValue = encodeURIComponent(value);
      paramsList.push(`${encodeKey}=${encodeValue}`);
    }
    if (paramsList.length <= 0) {
      return "";
    }
    return `?${paramsList.join("&")}`;
  }
}

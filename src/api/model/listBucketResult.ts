import { Content } from "./content";
import { CommonPrefix } from "./commonPrefix";

export interface ListBucketResult {
  name: string;
  prefix?: string;
  maxKeys: number;
  keyCount: number;
  delimiter?: string;
  continuationToken?: string;
  nextContinuationToken?: string;
  isTruncated: boolean;
  contents: Content[];
  commonPrefixes: CommonPrefix[];
}

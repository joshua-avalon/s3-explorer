export interface Content {
  key: string;
  name: string;
  type?: string;
  isFolder: boolean;
  lastModified: Date;
  eTag: string;
  size: number;
  sizeStr: string;
  storageClass: string;
}

import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

import { ListBucketPage, RedirectSlash } from "@/component";

type OwnProps = {};
type ComponentProps = OwnProps;

export default class App extends Component<ComponentProps> {
  public render(): JSX.Element {
    return (
      <HashRouter>
        <Switch>
          <Route exact strict path="/:url*" component={RedirectSlash} />
          <Route exact path="/*" component={ListBucketPage} />
        </Switch>
      </HashRouter>
    );
  }
}

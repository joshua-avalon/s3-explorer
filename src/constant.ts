function isTrue(value: string | undefined): boolean {
  return value === "true";
}

export const SHOW_INPUT = isTrue(process.env.SHOW_INPUT);
export const API_URL: string = process.env.API_URL || "";
export const SHOW_RIBBON = isTrue(process.env.SHOW_RIBBON);

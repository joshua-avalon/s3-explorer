import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import { Breadcrumb } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import style from "./style.scss";

type OwnProps = { pathname: string };
type ComponentProps = OwnProps;

export default class Navigation extends PureComponent<ComponentProps> {
  public render(): JSX.Element {
    const { pathname } = this.props;
    const folders = pathname.split("/").filter(f => f);
    const items = folders.map((folder, index) => {
      const key = folders.slice(0, index + 1);
      return (
        <Breadcrumb.Item key={`${index}-${folder}`}>
          <Link to={`/${key.join("/")}/`}>{folder}</Link>
        </Breadcrumb.Item>
      );
    });
    return (
      <Breadcrumb className={style.nav}>
        <Breadcrumb.Item>
          <Link to="/" aria-label="Home">
            <FontAwesomeIcon icon="home" />
          </Link>
        </Breadcrumb.Item>
        {items}
      </Breadcrumb>
    );
  }
}

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCalendar,
  faDownload,
  faFile,
  faFileAlt,
  faFileArchive,
  faFileAudio,
  faFileCode,
  faFileCsv,
  faFileImage,
  faFilePdf,
  faFilePowerpoint,
  faFileVideo,
  faFileWord,
  faFolder,
  faHome,
  faMapMarkerAlt,
  faSdCard,
  faSyncAlt,
  faTag,
  faTimesCircle
} from "@fortawesome/free-solid-svg-icons";
import { faAws } from "@fortawesome/free-brands-svg-icons";

library.add(
  faCalendar,
  faDownload,
  faFile,
  faFileAlt,
  faFileArchive,
  faFileAudio,
  faFileCode,
  faFileCsv,
  faFileImage,
  faFilePdf,
  faFilePowerpoint,
  faFileVideo,
  faFileWord,
  faFolder,
  faHome,
  faMapMarkerAlt,
  faSdCard,
  faSyncAlt,
  faTag,
  faTimesCircle,
  faAws
);

export default library;

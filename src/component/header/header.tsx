import React, { ChangeEvent, PureComponent } from "react";
import { connect } from "react-redux";
import { Input, Layout, Menu } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { SHOW_INPUT } from "@/constant";
import { Dispatch, State } from "@/store/type";
import { selectApiUrl, updateConfig } from "@/store/config";

import style from "./style.scss";

/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mapStateToProps = (state: State) => ({
  apiUrl: selectApiUrl(state)
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
  updateConfig: (apiUrl: string) => dispatch(updateConfig(apiUrl))
});
/* eslint-enable @typescript-eslint/explicit-function-return-type */

type OwnProps = {
  onRefresh: () => void;
};
type OwnState = {
  apiUrl: string;
};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type ComponentProps = OwnProps & StateProps & DispatchProps;

class Header extends PureComponent<ComponentProps, OwnState> {
  public constructor(props: ComponentProps) {
    super(props);
    const { apiUrl } = this.props;
    this.state = {
      apiUrl
    };
  }

  protected updateApiUrl = (event: ChangeEvent<HTMLInputElement>) => {
    if (!SHOW_INPUT) {
      return;
    }
    const { apiUrl, updateConfig } = this.props;
    const value = event.target.value.replace(/\/$/u, "");
    if (value !== apiUrl) {
      updateConfig(value);
    }
  };

  protected onApiUrlChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { apiUrl } = this.state;
    const value = event.target.value.replace(/\/$/u, "");
    if (value !== apiUrl) {
      this.setState({ apiUrl: value });
    }
  };

  public componentDidUpdate(prevProps: ComponentProps): void {
    const { apiUrl } = this.props;
    if (apiUrl !== prevProps.apiUrl) {
      this.setState({ apiUrl });
    }
  }

  public render(): JSX.Element {
    const { onRefresh } = this.props;
    const { apiUrl } = this.state;
    const visibility = SHOW_INPUT ? "visible" : "hidden";
    return (
      <Layout.Header className={style.header}>
        <div className={style.headerInner}>
          <div className={style.title}>
            <FontAwesomeIcon icon={["fab", "aws"]} className={style.logo} />
          </div>
          <div className={style.input} style={{ visibility }}>
            <Input
              onInput={this.updateApiUrl}
              onChange={this.onApiUrlChange}
              value={apiUrl}
              type="url"
              prefix={
                <FontAwesomeIcon icon="map-marker-alt" className={style.icon} />
              }
              placeholder="https://bucket.s3.amazonaws.com"
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
              spellCheck={false}
              aria-label="S3 bucket URL"
            />
          </div>
          <Menu
            theme="light"
            mode="horizontal"
            selectable={false}
            className={style.menu}>
            <Menu.Item key="1" onClick={onRefresh}>
              <FontAwesomeIcon icon="sync-alt" />
            </Menu.Item>
          </Menu>
        </div>
      </Layout.Header>
    );
  }
}

export default connect<StateProps, DispatchProps, OwnProps, State>(
  mapStateToProps,
  mapDispatchToProps
)(Header);

import React, { PureComponent } from "react";

import style from "./style.scss";

export default class Ribbon extends PureComponent {
  public render(): JSX.Element {
    return (
      <a
        className={`${style.ribbon} ${style.leftTop}`}
        href="https://gitlab.com/joshuaavalon/s3-explorer"
        data-ribbon="Fork me on GitLab"
        title="Fork me on GitLab">
        Fork me on GitLab
      </a>
    );
  }
}

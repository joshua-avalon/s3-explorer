import React, { PureComponent } from "react";
import { Redirect } from "react-router-dom";
import { RouteComponentProps, withRouter } from "react-router";

type OwnProps = {};
type ComponentProps = OwnProps & RouteComponentProps;

class RedirectSlash extends PureComponent<ComponentProps> {
  public render(): JSX.Element {
    const { pathname } = this.props.location;
    return <Redirect to={`${pathname}/`} />;
  }
}

export default withRouter(RedirectSlash);

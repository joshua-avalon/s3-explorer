import React, { PureComponent } from "react";
import { Layout } from "antd";

import style from "./style.scss";

type OwnProps = {};
type ComponentProps = OwnProps;

export default class Footer extends PureComponent<ComponentProps> {
  public render(): JSX.Element {
    return (
      <Layout.Footer className={style.footer}>
        <a href="https://gitlab.com/joshuaavalon/s3-explorer">S3 Explorer</a>
        ©2019 Created by <a href="https://joshuaavalon.io">Joshua Avalon</a>
      </Layout.Footer>
    );
  }
}

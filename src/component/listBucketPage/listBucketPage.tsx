import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { Layout } from "antd";

import { Dispatch, State } from "@/store/type";
import {
  isS3PrefixLoading,
  listBucket,
  selectS3PrefixResult
} from "@/store/s3";
import { SHOW_RIBBON } from "@/constant";
import { selectApiUrl, updateConfig } from "@/store/config";
import { Header } from "@/component/header";
import { Footer } from "@/component/footer";
import { Ribbon } from "@/component/ribbon";
import { Navigation } from "@/component/navigation";
import { BucketList } from "@/component/bucketList";

import style from "./style.scss";

/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mapStateToProps = (state: State, props: OwnProps) => {
  const { pathname } = props.location;
  const prefix = pathname.replace(/^\//u, "");
  const apiUrl = selectApiUrl(state);
  return {
    prefix,
    apiUrl,
    loading: isS3PrefixLoading(state, { prefix }),
    result: selectS3PrefixResult(state, { prefix })
  };
};
const mapDispatchToProps = (dispatch: Dispatch, props: OwnProps) => {
  const { pathname } = props.location;
  const prefix = pathname.replace(/^\//u, "");
  return {
    listBucket: (force: boolean = false) => dispatch(listBucket(prefix, force)),
    updateConfig: (apiUrl: string) => dispatch(updateConfig(apiUrl))
  };
};
/* eslint-enable @typescript-eslint/explicit-function-return-type */

type OwnProps = {} & RouteComponentProps;
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type ComponentProps = OwnProps & StateProps & DispatchProps;

class ListBucketPage extends PureComponent<ComponentProps> {
  public componentDidMount(): void {
    const { listBucket, updateConfig, location } = this.props;
    const query = new URLSearchParams(location.search);
    const apiUrl = query.get("api");
    if (apiUrl) {
      updateConfig(apiUrl);
    }
    listBucket();
  }

  public componentDidUpdate(prevProps: ComponentProps): void {
    const { listBucket, location } = this.props;
    if (location !== prevProps.location) {
      listBucket();
    }
  }

  protected refresh = (): void => {
    const { listBucket } = this.props;
    listBucket(true);
  };

  public render(): JSX.Element {
    const { prefix, location } = this.props;
    const { pathname } = location;
    const ribbon = SHOW_RIBBON ? <Ribbon /> : null;
    return (
      <Layout className={style.layout}>
        <Header onRefresh={this.refresh} />
        <Layout.Content className={style.content}>
          {ribbon}
          <Navigation pathname={pathname} />
          <div className={style.innerContent}>
            <BucketList prefix={prefix} />
          </div>
        </Layout.Content>
        <Footer />
      </Layout>
    );
  }
}

export default withRouter(
  connect<StateProps, DispatchProps, OwnProps, State>(
    mapStateToProps,
    mapDispatchToProps
  )(ListBucketPage)
);

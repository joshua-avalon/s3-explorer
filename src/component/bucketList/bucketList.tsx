import React, { PureComponent, ReactNode } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button, List } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import moment from "moment";

import { Dispatch, State } from "@/store/type";
import {
  isS3PrefixHasMore,
  isS3PrefixLoading,
  listBucket,
  loadMoreBucket,
  selectS3PrefixResult
} from "@/store/s3";
import { selectApiUrl } from "@/store/config";
import { Content } from "@/api";

import style from "./style.scss";

const textTypes = ["txt", "log", "rtf", "xml", "yml", "yaml", "json"];
const archiveTypes = ["zip", "7z", "rar", "tar", "gz"];
const codeTypes = [
  "html",
  "js",
  "py",
  "c",
  "cpp",
  "java",
  "kt",
  "php",
  "htm",
  "sh"
];
const audioTypes = ["mp3", "wav", "flac", "m4a"];
const videoTypes = ["mp4", "mkv", "avi", "webm", "flv", "wmv"];
const imageTypes = ["jpg", "jpeg", "png", "gif", "webp"];

/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mapStateToProps = (state: State, props: OwnProps) => ({
  loading: isS3PrefixLoading(state, props),
  result: selectS3PrefixResult(state, props),
  hasMore: isS3PrefixHasMore(state, props),
  apiUrl: selectApiUrl(state)
});
const mapDispatchToProps = (dispatch: Dispatch, props: OwnProps) => ({
  listBucket: () => dispatch(listBucket(props.prefix)),
  loadMoreBucket: () => dispatch(loadMoreBucket(props.prefix))
});
/* eslint-enable @typescript-eslint/explicit-function-return-type*/

type OwnProps = { prefix: string };
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type ComponentProps = OwnProps & StateProps & DispatchProps;

class BucketList extends PureComponent<ComponentProps> {
  protected renderItem = (content: Content) => {
    let title: ReactNode | undefined;
    const { apiUrl } = this.props;
    if (!content.isFolder && apiUrl) {
      const href = `${apiUrl}/${content.key}`;
      title = <a href={href}>{content.name}</a>;
    } else if (content.isFolder && apiUrl) {
      const folderRegex = /_\$folder\$$/u;
      const path = "/" + content.key.replace(folderRegex, "/");
      title = <Link to={path}>{content.name}</Link>;
    }
    let avatar: ReactNode | undefined;
    const formattedDate = moment(content.lastModified).format(
      "MMMM Do YYYY, h:mm:ss a"
    );

    const actions: ReactNode[] = [];
    actions.push(
      <span title="Last Modified" className={style.actionSpan}>
        <FontAwesomeIcon
          className={style.actionIcon}
          icon="calendar"
          key="calendar"
        />
        {formattedDate}
      </span>
    );
    if (content.isFolder) {
      avatar = <FontAwesomeIcon className={style.icon} icon="folder" />;
    } else {
      actions.push(
        <span title="Size" className={style.actionSpan}>
          <FontAwesomeIcon
            className={style.actionIcon}
            icon="sd-card"
            key="size"
          />
          {content.sizeStr}
        </span>
      );
      if (apiUrl) {
        const href = `${apiUrl}/${content.key}`;
        actions.push(
          <span title="Download" className={style.actionSpan}>
            <a href={href}>
              <FontAwesomeIcon
                className={style.actionIcon}
                icon="download"
                key="download"
              />
            </a>
          </span>
        );
      }
      const avatarIcon: IconProp = this.iconByType(content.type);
      avatar = <FontAwesomeIcon className={style.icon} icon={avatarIcon} />;
    }
    const desc = (
      <span title="ETag">
        <FontAwesomeIcon className={style.actionIcon} icon="tag" />
        {content.eTag}
      </span>
    );
    return (
      <List.Item actions={actions}>
        <List.Item.Meta avatar={avatar} title={title} description={desc} />
      </List.Item>
    );
  };

  protected iconByType = (type?: string): IconProp => {
    if (!type) {
      return "file";
    }
    if (textTypes.includes(type)) {
      return "file-alt";
    }
    if (["doc", "docx"].includes(type)) {
      return "file-word";
    }
    if (archiveTypes.includes(type)) {
      return "file-archive";
    }
    if (audioTypes.includes(type)) {
      return "file-audio";
    }
    if (codeTypes.includes(type)) {
      return "file-code";
    }
    if (["csv"].includes(type)) {
      return "file-csv";
    }
    if (["pdf"].includes(type)) {
      return "file-pdf";
    }
    if (imageTypes.includes(type)) {
      return "file-image";
    }
    if (["ppt", "pptx"].includes(type)) {
      return "file-powerpoint";
    }
    if (videoTypes.includes(type)) {
      return "file-video";
    }
    return "file";
  };

  public render(): JSX.Element {
    const { loading, result, hasMore, loadMoreBucket } = this.props;
    let dataSource: Content[] = [];
    if (result) {
      dataSource = result.contents;
    }
    const loadMore = hasMore ? (
      <div className={style.loadMore}>
        <Button onClick={loadMoreBucket}>loading more</Button>
      </div>
    ) : null;
    return (
      <List
        loading={loading}
        itemLayout="vertical"
        size="large"
        loadMore={loadMore}
        dataSource={dataSource}
        renderItem={this.renderItem}
      />
    );
  }

  public componentDidUpdate(prevProps: ComponentProps): void {
    const { listBucket, apiUrl } = this.props;
    if (prevProps.apiUrl !== apiUrl) {
      listBucket();
    }
  }
}

export default connect<StateProps, DispatchProps, OwnProps, State>(
  mapStateToProps,
  mapDispatchToProps
)(BucketList);

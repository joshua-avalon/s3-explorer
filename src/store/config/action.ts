import { ActionsUnion, Dispatch, DispatchAction } from "@/store/type";
import { createAction } from "@/store/action";

export enum ActionTypes {
  UPDATE_CONFIG = "UPDATE_CONFIG"
}

export const Actions = {
  setApiUrl: (apiUrl: string) =>
    createAction(ActionTypes.UPDATE_CONFIG, { apiUrl })
};

export type Actions = ActionsUnion<typeof Actions>;

export function updateConfig(apiUrl: string): DispatchAction {
  return async (dispatch: Dispatch) => {
    dispatch(Actions.setApiUrl(apiUrl));
    return Promise.resolve();
  };
}

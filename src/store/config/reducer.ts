import { Actions } from "@/store/type";
import { API_URL } from "@/constant";

import { ActionTypes } from "./action";

export type State = {
  apiUrl: string;
};
const initialState: State = {
  apiUrl: API_URL
};

export default function updateConfig(
  state: State = initialState,
  action: Actions
): State {
  switch (action.type) {
    case ActionTypes.UPDATE_CONFIG:
      return { ...action.payload };
    default:
      return state;
  }
}

import { State } from "@/store/type";

export const selectApiUrl = (state: State): string => state.config.apiUrl;

import { Actions } from "@/store/type";

import { ListBucketResult } from "@/api";
import * as Config from "@/store/config";

import { ActionTypes } from "./action";

export type State = {
  [key: string]: PrefixState;
};
export type PrefixState = {
  loading: boolean;
  result: ListBucketResult | undefined;
};
const initialState: State = {};
const initialPrefixState: PrefixState = {
  loading: false,
  result: undefined
};

function listPrefixBucket(
  state: PrefixState = initialPrefixState,
  action: Actions
): PrefixState {
  switch (action.type) {
    case ActionTypes.LOAD_MORE_BUCKET:
    case ActionTypes.LIST_BUCKET:
      return { ...state, loading: true };
    case ActionTypes.LIST_BUCKET_SUCCESS:
      return {
        loading: false,
        result: action.payload.result
      };
    case ActionTypes.LOAD_MORE_BUCKET_SUCCESS: {
      const { result } = action.payload;
      if (state.result) {
        result.keyCount += state.result.keyCount;
        result.maxKeys += state.result.maxKeys;
        result.contents = [...state.result.contents, ...result.contents];
        result.commonPrefixes = [
          ...state.result.commonPrefixes,
          ...result.commonPrefixes
        ];
      }
      return { loading: false, result };
    }
    case ActionTypes.LOAD_MORE_BUCKET_FAILURE:
      return { ...state, loading: false };
    case ActionTypes.LIST_BUCKET_FAILURE:
      return {
        loading: false,
        result: undefined
      };
    default:
      return state;
  }
}

export default function listBucket(
  state: State = initialState,
  action: Actions
): State {
  switch (action.type) {
    case ActionTypes.LIST_BUCKET:
    case ActionTypes.LIST_BUCKET_SUCCESS:
    case ActionTypes.LIST_BUCKET_FAILURE:
    case ActionTypes.LOAD_MORE_BUCKET:
    case ActionTypes.LOAD_MORE_BUCKET_SUCCESS:
    case ActionTypes.LOAD_MORE_BUCKET_FAILURE: {
      const { prefix } = action.payload;
      return {
        ...state,
        [prefix]: listPrefixBucket(state[prefix], action)
      };
    }
    case Config.ActionTypes.UPDATE_CONFIG:
      return initialState;
    default:
      return state;
  }
}

import { createSelector } from "reselect";

import { State } from "@/store/type";

import { State as S3State } from "./reducer";

export const selectS3 = (state: State): S3State => state.s3;

type S3PrefixParams = {
  prefix: string;
};

export const selectS3Prefix = createSelector(
  selectS3,
  (_: State, props: S3PrefixParams) => props.prefix,
  (s3, prefix) => s3[prefix] || { loading: false, result: undefined }
);

export const isS3PrefixLoading = createSelector(
  selectS3Prefix,
  s3Prefix => s3Prefix.loading
);

export const selectS3PrefixResult = createSelector(
  selectS3Prefix,
  s3Prefix => s3Prefix.result
);

export const isS3PrefixLoaded = createSelector(
  selectS3PrefixResult,
  s3Prefix => s3Prefix !== undefined
);

export const selectNextContinuationToken = createSelector(
  selectS3PrefixResult,
  s3Prefix => {
    if (!s3Prefix) {
      return undefined;
    }
    return s3Prefix.nextContinuationToken;
  }
);

export const isS3PrefixHasMore = createSelector(
  selectNextContinuationToken,
  token => token !== undefined
);

import { ActionsUnion, Dispatch, DispatchAction, State } from "@/store/type";
import { createAction } from "@/store/action";

import { Api, ListBucketResult } from "@/api";
import { selectApiUrl } from "@/store/config";

import {
  isS3PrefixLoaded,
  isS3PrefixLoading,
  selectNextContinuationToken
} from "./select";

export enum ActionTypes {
  LIST_BUCKET = "LIST_BUCKET",
  LIST_BUCKET_SUCCESS = "LIST_BUCKET_SUCCESS",
  LIST_BUCKET_FAILURE = "LIST_BUCKET_FAILURE",
  LOAD_MORE_BUCKET = "LOAD_MORE_BUCKET",
  LOAD_MORE_BUCKET_SUCCESS = "LOAD_MORE_BUCKET_SUCCESS",
  LOAD_MORE_BUCKET_FAILURE = "LOAD_MORE_BUCKET_FAILURE"
}

export const Actions = {
  listBucket: (prefix: string) =>
    createAction(ActionTypes.LIST_BUCKET, { prefix }),
  listBucketSuccess: (prefix: string, result: ListBucketResult) =>
    createAction(ActionTypes.LIST_BUCKET_SUCCESS, { prefix, result }),
  listBucketFailure: (prefix: string, message?: string) =>
    createAction(ActionTypes.LIST_BUCKET_FAILURE, { prefix, message }),
  loadMoreBucket: (prefix: string) =>
    createAction(ActionTypes.LOAD_MORE_BUCKET, { prefix }),
  loadMoreBucketSuccess: (prefix: string, result: ListBucketResult) =>
    createAction(ActionTypes.LOAD_MORE_BUCKET_SUCCESS, { prefix, result }),
  loadMoreBucketFailure: (prefix: string, message?: string) =>
    createAction(ActionTypes.LOAD_MORE_BUCKET_FAILURE, { prefix, message })
};

export type Actions = ActionsUnion<typeof Actions>;

export function listBucket(
  prefix: string,
  force: boolean = false
): DispatchAction {
  return async (dispatch: Dispatch, getState: () => State) => {
    const state = getState();
    const params = { prefix };
    if (
      isS3PrefixLoading(state, params) ||
      (isS3PrefixLoaded(state, params) && !force)
    ) {
      return Promise.resolve();
    }
    const apiUrl = selectApiUrl(state);
    if (!apiUrl) {
      return Promise.resolve();
    }
    const api = new Api(apiUrl);
    dispatch(Actions.listBucket(prefix));
    return api
      .list(params)
      .then(result => {
        dispatch(Actions.listBucketSuccess(prefix, result));
        return Promise.resolve();
      })
      .catch((error: Error) => {
        dispatch(Actions.listBucketFailure(prefix, error.message));
      });
  };
}

export function loadMoreBucket(prefix: string): DispatchAction {
  return async (dispatch: Dispatch, getState: () => State) => {
    const state = getState();
    const params = { prefix };
    const token = selectNextContinuationToken(state, params);
    if (!token || isS3PrefixLoading(state, params)) {
      return Promise.resolve();
    }
    const apiUrl = selectApiUrl(state);
    if (!apiUrl) {
      return Promise.reject(Error("apiUrl is empty!"));
    }
    const api = new Api(apiUrl);
    dispatch(Actions.loadMoreBucket(prefix));
    return api
      .list(params)
      .then(result => {
        dispatch(Actions.loadMoreBucketSuccess(prefix, result));
        return Promise.resolve();
      })
      .catch((error: Error) => {
        dispatch(Actions.loadMoreBucketFailure(prefix, error.message));
      });
  };
}

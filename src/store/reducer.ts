import { combineReducers, Reducer } from "redux";

import { Action, State } from "./type";
import { reducer as s3 } from "./s3";
import { reducer as config } from "./config";

const reducer: Reducer<State, Action> = combineReducers({ config, s3 });

export default reducer;

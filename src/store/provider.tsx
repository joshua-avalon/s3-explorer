import React, { StatelessComponent } from "react";
import { applyMiddleware, compose, createStore } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";

import reducer from "./reducer";
import { Store } from "./type";

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose;

const store: Store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

export const StoreProvider: StatelessComponent = ({
  children
}): JSX.Element => <Provider store={store}>{children}</Provider>;

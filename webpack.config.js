module.exports = (env, args) => {
  return args.mode === "production"
    ? require("./config/webpack.config.prod")(env, args)
    : require("./config/webpack.config.dev")(env, args);
};
